import React from 'react';
import {View, Text, Button} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FingerprintScanner from 'react-native-fingerprint-scanner';

const AuthScreen = (props) => {
  const fingerAuth = () => {
    FingerprintScanner.authenticate({title: 'Continue with Finger-Sprint'})
      .then(() => {
        props.navigation.navigate('Home');
      })
      .catch((err) => console.log(err));
  };

  return (
    <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          marginBottom: 50,
          width: '50%',
        }}>
        <View>
          <Ionicons onPress={fingerAuth} name="finger-print" size={50} />
        </View>
        <MaterialCommunityIcons name="face-recognition" size={50} />
      </View>
      <Text>Use Finger-sprint or Face-detection to continue</Text>
    </View>
  );
};

export default AuthScreen;
