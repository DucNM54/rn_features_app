import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from '../screens/HomeScreen';
import AuthScreen from '../screens//AuthScreen';
import NotificationScreen from '../screens/NotificationScreen';
import LocationScreen from '../screens/LocationScreen';
import CameraScreen from '../screens/CameraScreen';
import FileSystem from '../screens/FileSystem';
import ContactsScreen from '../screens/ContactsScreen';
import SmsScreen from '../screens/SmsScreen';
import CameraHandler from '../components/CameraHandler';
import Icon from 'react-native-vector-icons/AntDesign';

const Stack = createStackNavigator();

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Auth"
          component={AuthScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{headerLeft: null}}
        />
        <Stack.Screen name="Notification" component={NotificationScreen} />
        <Stack.Screen name="Location" component={LocationScreen} />
        <Stack.Screen
          name="Camera"
          component={CameraScreen}
          options={({navigation}) => {
            return {
              headerRight: () => (
                <Icon
                  style={{marginRight: 10}}
                  name="camera"
                  size={30}
                  color="#000"
                  onPress={() => {
                    navigation.navigate('CameraHandler');
                  }}
                />
              ),
            };
          }}
        />
        <Stack.Screen name="FileSystem" component={FileSystem} />
        <Stack.Screen name="Contacts" component={ContactsScreen} />
        <Stack.Screen name="Sms" component={SmsScreen} />
        <Stack.Screen name="CameraHandler" component={CameraHandler} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
