/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet, StatusBar} from 'react-native';
import AppNavigator from './navigation/AppNavigatior';
// import {createStore, combineReducers, applyMiddleware} from 'redux';
// import {Provider} from 'react-redux';
// import ReduxThunk from 'redux-thunk';

// const rootReducer = combineReducers({});
// const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

const App: () => React$Node = () => {
  return (
    // <Provider store={store}>
      <AppNavigator />
    // </Provider>
  );
};

export default App;
